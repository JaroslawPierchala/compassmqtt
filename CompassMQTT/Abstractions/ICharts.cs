﻿using ChartJSCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompassMQTT.Abstractions
{
	public interface ICharts
	{
		Chart getLineChart(List<double?> inputDataset, List<string> inputLabels, string name);
		Chart getBarChart(List<double?> inputDataset, List<string> inputLabels, string name);
		Chart getPieChart(List<double?> inputDataset, List<string> inputLabels, string name);

		Chart getDoubleLineChart(List<double?> inputDataset1, List<double?> inputDatase2, List<string> inputLabels, string name1, string name2);
		Chart getDoubleBarChart(List<double?> inputDataset1, List<double?> inputDatase2, List<string> inputLabels, string name1, string name2);
	}
}
