﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompassMQTT.Models
{
	public class CompassModel
	{
		public Feed[] feeds { get; set; }
	}

	public class Feed
	{
		public DateTime created_at { get; set; }
		public string field1 { get; set; }
		public string field2 { get; set; }
		public string field3 { get; set; }
	}

}
