﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompassMQTT.Models
{
	public class CompassViewModel
	{
		public DateTime created_at { get; set; }
		public double? X { get; set; }
		public double? Y { get; set; }
		public double? Z { get; set; }
	}
}
