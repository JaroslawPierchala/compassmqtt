﻿using ChartJSCore.Helpers;
using ChartJSCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompassMQTT.Abstractions;

namespace CompassMQTT.Services
{
	public class Charts : ICharts
	{
        public Chart getLineChart(List<double?> inputDataset, List<string> inputLabels, string name)
        {
            Chart chart = new Chart();

            chart.Type = Enums.ChartType.Line;

            ChartJSCore.Models.Data data = new ChartJSCore.Models.Data();

            data.Labels = inputLabels;

            LineDataset dataset = new LineDataset()
            {
                Label = name,
                Data = inputDataset,
                Fill = "false",
                LineTension = 0.1,
                BackgroundColor = ChartColor.FromHexString("#0eaaff"),
                BorderColor = ChartColor.FromHexString("#0c79ff"),
                BorderCapStyle = "butt",
                BorderDash = new List<int> { },
                BorderDashOffset = 0.0,
                BorderJoinStyle = "miter",
                PointRadius = new List<int> { 3 },
                SpanGaps = false
            };

            data.Datasets = new List<Dataset>();
            data.Datasets.Add(dataset);
            chart.Data = data;

            return chart;
        }

        public Chart getBarChart(List<double?> inputDataset, List<string> inputLabels, string name)
        {
            Chart chart = new Chart();

            chart.Type = Enums.ChartType.Bar;

            ChartJSCore.Models.Data data = new ChartJSCore.Models.Data();
            data.Labels = inputLabels;

            BarDataset dataset = new BarDataset()
            {
                Label = name,
                Data = inputDataset,
                BackgroundColor = new List<ChartColor>()
                {
                ChartColor.FromHexString("#0367A6")
                },
                BorderColor = new List<ChartColor>()
                {
                ChartColor.FromHexString("#0367A6")
                },
                BorderWidth = new List<int>() { 1 }
            };

            data.Datasets = new List<Dataset>();
            data.Datasets.Add(dataset);
            chart.Data = data;

            return chart;
        }

        public Chart getPieChart(List<double?> inputDataset, List<string> inputLabels, string name)
        {
            Chart chart = new Chart();
            chart.Type = Enums.ChartType.Pie;

            ChartJSCore.Models.Data data = new ChartJSCore.Models.Data();
            data.Labels = inputLabels;

            PieDataset dataset = new PieDataset()
            {
                Label = name,
                BackgroundColor = new List<ChartColor>() { ChartColor.FromHexString("#FF6384"), ChartColor.FromHexString("#36A2EB"), ChartColor.FromHexString("#FFCE56") },
                HoverBackgroundColor = new List<ChartColor>() { ChartColor.FromHexString("#FF6384"), ChartColor.FromHexString("#36A2EB"), ChartColor.FromHexString("#FFCE56") },
                Data = inputDataset
            };

            data.Datasets = new List<Dataset>();
            data.Datasets.Add(dataset);

            chart.Data = data;

            return chart;
        }

        public Chart getDoubleLineChart(List<double?> inputDataset1, List<double?> inputDatase2, List<string> inputLabels, string name1, string name2)
        {
            Chart chart = new Chart();

            chart.Type = Enums.ChartType.Line;

            ChartJSCore.Models.Data data = new ChartJSCore.Models.Data();

            data.Labels = inputLabels;

            LineDataset dataset = new LineDataset()
            {
                Label = name1,
                Data = inputDataset1,
                Fill = "true",
                LineTension = 0.1,
                BackgroundColor = ChartColor.FromRgba(14,170,255,0.3),
                BorderColor = ChartColor.FromHexString("#0c79ff"),
                BorderCapStyle = "butt",
                BorderDash = new List<int> { },
                BorderDashOffset = 0.0,
                BorderJoinStyle = "miter",
                PointRadius = new List<int> { 0 },
                SpanGaps = false
            };

            LineDataset dataset2 = new LineDataset()
            {
                Label = name2,
                Data = inputDatase2,
                Fill = "true",
                LineTension = 0.1,
                BackgroundColor = ChartColor.FromRgba(255,0,0,0.3),
                BorderColor = ChartColor.FromHexString("#ff0000"),
                BorderCapStyle = "butt",
                BorderDash = new List<int> { },
                BorderDashOffset = 0.0,
                BorderJoinStyle = "miter",
                PointRadius = new List<int> { 0 },
                SpanGaps = false
            };


            data.Datasets = new List<Dataset>();
            data.Datasets.Add(dataset);
            data.Datasets.Add(dataset2);
            chart.Data = data;

            return chart;
        }

        public Chart getDoubleBarChart(List<double?> inputDataset1, List<double?> inputDatase2, List<string> inputLabels, string name1, string name2)
        {
            Chart chart = new Chart();

            chart.Type = Enums.ChartType.Bar;

            ChartJSCore.Models.Data data = new ChartJSCore.Models.Data();
            data.Labels = inputLabels;

            BarDataset dataset = new BarDataset()
            {
                Label = name1,
                Data = inputDataset1,
                BackgroundColor = new List<ChartColor>()
                {
                ChartColor.FromHexString("#0c79ff")
                },
                BorderColor = new List<ChartColor>()
                {
                ChartColor.FromRgba(0,0,0,0)
                },
                BorderWidth = new List<int>() { 1 }
            };

            BarDataset dataset2 = new BarDataset()
            {
                Label = name2,
                Data = inputDatase2,
                BackgroundColor = new List<ChartColor>()
                {
                ChartColor.FromHexString("#ff0000")
                },
                BorderColor = new List<ChartColor>()
                {
                ChartColor.FromRgba(0,0,0,0)
                },
                BorderWidth = new List<int>() { 1 }
            };

            data.Datasets = new List<Dataset>();
            data.Datasets.Add(dataset);
            data.Datasets.Add(dataset2);
            chart.Data = data;

            return chart;
        }
    }
}
