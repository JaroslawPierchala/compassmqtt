﻿using CompassMQTT.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using CompassMQTT.Models;

namespace CompassMQTT.Controllers
{
	public class CompassController : Controller
	{
		private readonly ICharts _charts;
		private string url = "https://api.thingspeak.com/channels/1236818/feeds.json?api_key=W4GDVI0MFIYV9S0A";

		public CompassController(ICharts charts)
		{
			_charts = charts;
		}

		public async Task<ActionResult> IndexAsync()
		{
			HttpClient client = new HttpClient();
			string jsonFile = await client.GetStringAsync(url);
			CompassModel compassData = JsonConvert.DeserializeObject<CompassModel>(jsonFile);

			List<CompassViewModel> compassList = new List<CompassViewModel>();
			foreach (var item in compassData.feeds)
			{
				compassList.Add(new CompassViewModel {
					created_at = item.created_at,
					X = double.Parse(item.field1),
					Y = double.Parse(item.field2),
					Z = double.Parse(item.field3)
				});
			}
			ViewData[$"CompassDataX"] = _charts.getLineChart(compassList.Select(x => x.X).ToList(), compassList.Select(x => x.created_at.ToString()).ToList(), "X");
			ViewData[$"CompassDataY"] = _charts.getLineChart(compassList.Select(x => x.Y).ToList(), compassList.Select(x => x.created_at.ToString()).ToList(), "Y");
			ViewData[$"CompassDataZ"] = _charts.getLineChart(compassList.Select(x => x.Z).ToList(), compassList.Select(x => x.created_at.ToString()).ToList(), "Z");
			return View();
		}
	}
}
