#include <Wire.h> // Import wire library
#include <Arduino.h>
#include "ThingSpeak.h"
#include <ESP8266WiFi.h>
#define MAG_ADDR  0x0E //i2C Address 7Bit address


char ssid[] = "Router";  
char pass[] = "XXXXXX";       
WiFiClient  client;

unsigned long myChannelNumber = 1236818; //id kanału
const char * myWriteAPIKey = "D72YFGD6ZG2QVDYF"; // API

String myStatus = "";

int X = 0;  
int Y = 0;  
int Z = 0; 

void config()
{
  Wire.beginTransmission(MAG_ADDR); // Transmit to i2c device 0x0E
  Wire.write(0x11);             // control the register 2
  Wire.write(0x80);             // set to write 0x80 to enable auto resets
  Wire.endTransmission();       // set to stop transmitting
  delay(15); 
  Wire.beginTransmission(MAG_ADDR); // set to transmit i2c device 0x0E
  Wire.write(0x10);             // control register 1
  Wire.write(1);                // set to write 0x01 to enable active mode
  Wire.endTransmission();       // set tp stop transmitting
}

void setup() {
  Wire.begin();        // Start i2c bus this optional for master
  Serial.begin(9600);
  WiFi.mode(WIFI_STA); 
  ThingSpeak.begin(client); 
  config();    // Start MAG3110 on
  pinMode(LED_BUILTIN, OUTPUT);
}

int mag_read_register(int reg)
{
  int reg_val;
 
  Wire.beginTransmission(MAG_ADDR); //start transmit to i2c device address 0x0E
  Wire.write(reg);           
  Wire.endTransmission();      
  delayMicroseconds(2); //Delay for 1.3 to start and stop 
 
  Wire.requestFrom(MAG_ADDR, 1); // set to request 1 byte
  while(Wire.available())    // set the slave may write less than requested
  { 
    reg_val = Wire.read(); // starting reading the byte
  }
 
  return reg_val;
}
 
int mag_read_value(int msb_reg, int lsb_reg)
{
  int val_low, val_high;  //set variable to define the MSB and LSB
  val_high = mag_read_register(msb_reg);
  delayMicroseconds(2); 
  val_low = mag_read_register(lsb_reg);
  int out = (val_low|(val_high << 8)); //concatenate the MSB and LSB
  return out;
}

int read_x()
{
  return mag_read_value(0x01, 0x02);
}
 
int read_y()
{
  return mag_read_value(0x03, 0x04);
}
 
int read_z()
{
  return mag_read_value(0x05, 0x06);
}

void print_values()
{
  X = read_x();
  Y = read_y();  
  Z = read_z();  

  ThingSpeak.setField(1, X);
  ThingSpeak.setField(2, Y);
  ThingSpeak.setField(3, Z); 

  Serial.print("X = ");
  Serial.println(X);
  Serial.print("Y = ");  
  Serial.println(Y);   
  Serial.print("Z = ");  
  Serial.println(Z);
  Serial.println("");
}

void loop() {
   if(WiFi.status() != WL_CONNECTED){
    Serial.print("Łączenie z siecią... ");  
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass); 
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nPołączono.");
  }  
  print_values();

  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Aktualizacja kanału powiodła się.");
  }
  else{
    Serial.println("Błąd - " + String(x));
  }

  ThingSpeak.setStatus(myStatus);   
  delay(15000);
}
